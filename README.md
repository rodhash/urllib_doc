
urllib
========

    This library contains 5 modules
    - request       : open URLs
    - response      : used internally, not used directly
    - error         : request exceptions containing several error classes
    - parse         : many func. to break url into meaninful pieces
    - robotparser   : used to inspect robots.txt files for bots/crawlers permissions
